const axios = require('axios')

module.exports.templateTags = [{
  name: 'jwttoken',
  displayName: 'JWT Token',
  description: 'Generate a JWT token',
  args: [
    {
      displayName: 'url',
      description: 'url',
      type: 'string',
    },
    {
      displayName: '_username',
      description: '_username',
      type: 'string',
    },
    {
      displayName: '_password',
      description: '_password',
      type: 'string',
    },
  ],
  async run (context, url, _username, _password) {
    const data = {
      '_username': _username,
      '_password': _password,
    }
    let token = ''
    await axios.post(url, data)
      .then((response) => {
        token = response.data.token
        return token
    })

    return token

  }
}]
