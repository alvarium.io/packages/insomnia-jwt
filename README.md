# @alvarium/insomnia-plugin-jwt-tokengenerator

Generates a JWT token

![Screenshot](/readme-ss.png)

- **JWTurl**: the URL where the JWT is generated.
- **JWTuser**: the username.
- **JWTpassword**: the password.
